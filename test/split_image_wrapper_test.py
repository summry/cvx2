import os
import torch
import numpy as np
from torch import nn
from torchvision import transforms
from cvx2 import WidthBlock, DOWidthBlock
from torchvision.datasets import ImageFolder
from cvx2.wrapper import SplitImageClassifyModelWrapper
from model_wrapper import log_utils

imgsz = 28
data_dir = '/Users/summy/data/AI-face'

if __name__ == '__main__':
	transform = transforms.Compose([
		transforms.Resize((imgsz, imgsz)),  # 将所有图片resize到28x28
		transforms.ToTensor()
	])
	train_transform = transforms.Compose([
		transforms.Resize((imgsz, imgsz)),  # 将所有图片resize到28x28
		transforms.RandomAffine(degrees=(-10, 10), translate=(0.1, 0.1), scale=(0.9, 1.1), shear=(-10, 10)),
		transforms.ToTensor()
	])
	dataset = ImageFolder(root=data_dir, transform=transform)
	classes = dataset.classes
	
	model = nn.Sequential(
		DOWidthBlock(c1=3, c2=32),
		nn.MaxPool2d(kernel_size=2, stride=2),
		DOWidthBlock(c1=32, c2=32),
		nn.MaxPool2d(kernel_size=2, stride=2),
		nn.Flatten(),
		nn.Linear(in_features=32 * (imgsz >> 2) ** 2, out_features=1024),
		nn.Dropout(0.2),
		nn.SiLU(inplace=True),
		nn.Linear(in_features=1024, out_features=len(classes)),
	)
	
	wrapper = SplitImageClassifyModelWrapper(model)
	wrapper.train(data=dataset, val_size=0.2, epochs=3, train_transform=train_transform, batch_size=64, lr=0.0001)
	print(wrapper.evaluate(data=data_dir, transform=transform, batch_size=64))
	print(wrapper.classification_report(data=data_dir, transform=transform, batch_size=64))
	log_utils.info('跑完了')

